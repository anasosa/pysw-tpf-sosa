#Nombre del proyecto 
Sistema de reserva de tickets

#Funcionalidad del sistema
Esta API está destinada a un Sistema de reseva de tickets para eventos, tales como obras de teatro, recitales, charlas, funciones de cine, entre otros. Además 
permite la gestión de usuarios, eventos y las reservas realizadas por los usuarios para dichos eventos.

#Tecnologías utilizadas
-NodeJS
-Express
-MySQL

#Instalación
Para la instalaión de este sistema es necesario, en primer lugar, montar la base de datos, también se debe ejecutar el comando npm install o bien npm i para instalar
las librerías y, por último, ejecutar el comando npm run dev para poder correr el sistema.
