const authentication = async (req, res, next) => {
    const { AppDataSource } = require("../db");
    const jwt = require('./../../node_modules/jsonwebtoken');
    const jwtSecret = 'somesecrettoken';
    let token = req.headers["authorization"];

    if(!token){
        res.status(401).send({
            error: "Authentication token is required"
        })
        return
    }

    try{
        const user = jwt.verify(token.replace('Bearer ', ''), jwtSecret);
        const { username } = user;
        const userFound = await AppDataSource.query("SELECT * FROM user WHERE username = ?", [username]);
            
        if (userFound.length === 0) {
            return res.status(400).json({ msg: "User doesn't exists" });
        }

        next();
    }catch (error) {
        res.status(403).json({
        errors: [
            {
            msg: "Invalid token",
            },
        ],
        });
    }
 }

const bookingValid = async (req, res, next) => {
    const { AppDataSource } = require("../db");
    const { id, username } = req.params;
    const endpoint = req.headers["ep-type"];

    if(endpoint === "create"){
        const user = await AppDataSource.query("SELECT * FROM user WHERE username = ?", [username]);
        if(user.length === 0) {
            return res.status(400).json({ msg: "User doesn't exists" });
        }

        const event = await AppDataSource.query("SELECT * FROM event WHERE id = ?", [id]);
        if (event.length === 0) {
            return res.status(400).json({ msg: "Event doesn't exist" });
        }

        const cuposOcupados = await AppDataSource.query("SELECT COUNT(*) as tickets FROM booking WHERE id_event = ?", [id]);
        if(event[0].limite > 0){
            if (event[0].limite === parseInt(cuposOcupados[0].tickets)) {
                return res.status(400).json({ msg: "Event not available, no tickets left" });
            }
        }
    }else{
        const { id_event } = req.body;

        const booking = await AppDataSource.query("SELECT * FROM booking WHERE id = ?", [id]);
        if (booking.length === 0) {
            return res.status(400).json({ msg: "Booking doesn't exist" });
        }
        
        const event = await AppDataSource.query("SELECT * FROM event WHERE id = ?", [id_event]);
        if (event.length === 0) {
            return res.status(400).json({ msg: "Event doesn't exist" });
        }

        const cuposOcupados = await AppDataSource.query("SELECT COUNT(*) as tickets FROM booking WHERE id_event = ?", [id_event]);

        if(event[0].limite > 0){
            if (event[0].limite === parseInt(cuposOcupados[0].tickets)) {
                return res.status(400).json({ msg: "Event not available, no tickets left" });
            }
        }
    }
    next();
 }

 module.exports = {
    authentication,
    bookingValid
 }