import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, CreateDateColumn, UpdateDateColumn,
} from "typeorm";
  
  @Entity() // se puede pasar como parametro el nombre de tabla ej: 'eventsTable'
  export class Event extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column()
    nombre: string;

    @Column()
    descripcion: string;

    @Column()
    lugar: string;

    @Column()
    fechaHora: Date;

    @Column()
    gps: string;

    @Column()
    precio: number;

    @Column()
    limite: number;

    @Column()
    tipoEvento: string;
  
    @Column({ default: true })
    active: boolean;
  
    @CreateDateColumn()
    createdAt: Date;
  
    @UpdateDateColumn()
    updatedAt: Date;
  }