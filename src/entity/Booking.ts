import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, CreateDateColumn, UpdateDateColumn, Double,
  } from "typeorm";
    
    @Entity() // se puede pasar como parametro el nombre de tabla ej: 'bookingsTable'
    export class Booking extends BaseEntity {
      @PrimaryGeneratedColumn()
      id: number;
    
      @Column()
      id_event: number;
    
      @Column()
      id_user: number;
  
      @Column()
      precio: number;
  
      @Column()
      fechaHora: Date;

      @Column()
      lugar: string;

      @Column()
      gps: string;
    
      @Column({ default: true })
      active: boolean;
    
      @CreateDateColumn()
      createdAt: Date;
    
      @UpdateDateColumn()
      updatedAt: Date;
    }