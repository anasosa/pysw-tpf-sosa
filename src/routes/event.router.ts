import { Router } from "express";
import {
  getEvents,
  createEvent,
  updateEvent,
  deleteEvent,
} from "../controllers/event.controller";
const { authentication } = require('../middlewares/middlewares.js')

const router = Router();


router.get("/events", getEvents);
router.post("/events", authentication, createEvent);
router.put("/events/:id", authentication, updateEvent);
router.delete("/events/:id", authentication, deleteEvent);

export default router;