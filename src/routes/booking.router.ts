import { Router } from "express";
import {
  getBookings,
  createBooking,
  updateBooking,
  deleteBooking,
} from "../controllers/booking.controller";
const { authentication, bookingValid } = require('../middlewares/middlewares.js') 

const router = Router();

router.get("/bookings", authentication, getBookings);
router.post("/bookings/:id/:username", authentication, bookingValid, createBooking);
router.put("/bookings/:id", authentication, bookingValid, updateBooking);
router.delete("/bookings/:id", authentication, deleteBooking);

export default router;