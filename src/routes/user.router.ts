import { Router } from "express";
import {getUsers, getUser, updateUser, deleteUser} from "../controllers/user.controller";
import {signIn, signUp, protectedEndpoint, refresh } from '../controllers/user.controller'
import passport from 'passport'

const { authentication } = require('../middlewares/middlewares.js')
const router = Router();

router.get("/users", authentication, getUsers);
router.get("/users/:id", authentication, getUser);
router.put("/users/:id", authentication, updateUser);
router.delete("/users/:id", authentication, deleteUser);

//Login y registro de usuario (jwt)
router.post('/signup', signUp);
router.post('/signin', signIn);

router.post('/token', refresh);
router.post('/protected', passport.authenticate('jwt', { session: false }), protectedEndpoint);

export default router;