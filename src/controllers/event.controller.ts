import { Request, Response } from "express";
import { Event } from "../entity/Event";

interface EventBody {
    nombre: string;
    descripcion: string;
    lugar: string;
    fechaHora: Date;
    gps: string;
    precio: number;
    limite: number;
    tipoEvento: string;
}

export const getEvents = async (req: Request, res: Response) => {
  console.log('entrando...');
  try {
    const events = await Event.find();
    console.log('events: --->'), events;
    return res.json(events);
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};

export const createEvent = async (req: Request, res: Response) => {
  const { nombre, descripcion, lugar, fechaHora, gps, precio, limite, tipoEvento } = req.body;
  const event = new Event();
  event.nombre = nombre;
  event.descripcion = descripcion;
  event.lugar = lugar;
  event.fechaHora = fechaHora;
  event.gps = gps;
  event.precio = precio;
  event.limite = limite;
  event.tipoEvento = tipoEvento;
  await event.save();
  return res.json(event);
};

export const updateEvent = async (req: Request, res: Response) => {
  const { id } = req.params;

  try {
    let event = await Event.findOneBy({ id: parseInt(id) });
    if (!event) return res.status(404).json({ message: "Event not found" });

    await Event.update({ id: parseInt(id) }, req.body);

    event = await Event.findOneBy({ id: parseInt(id) });
    return res.status(200).json(event);
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};

export const deleteEvent = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const result = await Event.delete({ id: parseInt(id) });

    if (result.affected === 0)
      return res.status(404).json({ message: "Event not found" });

    return res.status(200).json({ message: "Event deleted successfully" });
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};