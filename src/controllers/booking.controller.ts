import { Request, Response } from "express";
import { Booking } from "../entity/Booking";
import { AppDataSource } from "../db";

interface BookingBody {
}

export const getBookings = async (req: Request, res: Response) => {
  console.log('entrando...');
  try {
    const bookings = await Booking.find();
    console.log('bookings: --->'), bookings;
    return res.json(bookings);
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};

export const createBooking = async (req: Request, res: Response) => {
  const { id, username } = req.params;
  
  const user = await AppDataSource.query("SELECT * FROM user WHERE username = ?", [username]);
  const event = await AppDataSource.query("SELECT * FROM event WHERE id = ?", [id]);

  const booking = new Booking();
  booking.id_event = parseInt(id);
  booking.id_user = user[0].id;
  booking.precio = event[0].precio;
  booking.fechaHora = event[0].fechaHora;
  booking.lugar = event[0].lugar;
  booking.gps = event[0].gps;

  await booking.save();
  return res.json(booking);
};

export const updateBooking = async (req: Request, res: Response) => {
  const { id } = req.params;
  const eventId = req.body.id_event;

  try {
    let booking = await Booking.findOneBy({ id: parseInt(id) });
    if (!booking) return res.status(404).json({ message: "Booking not found" });

    const event = await AppDataSource.query("SELECT * FROM event WHERE id = ?", [eventId]);

    await Booking.update({ id: parseInt(id) }, {
        id_event: parseInt(eventId),
        precio: event[0].precio,
        fechaHora: event[0].fechaHora,
        lugar: event[0].lugar,
        gps: event[0].gps
    });

    booking = await Booking.findOneBy({ id: parseInt(id) });
    return res.status(200).json(booking);
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};

export const deleteBooking = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const result = await Booking.delete({ id: parseInt(id) });

    if (result.affected === 0)
      return res.status(404).json({ message: "Booking not found" });

      return res.status(200).json({ message: "Booking deleted successfully" });
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};